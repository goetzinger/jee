package de.simplydevelop.hello;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Named("managedBean")
@SessionScoped
public class AuthenticationBean implements Serializable {

    public List<String> getItems(){
        return Arrays.asList("one", "two" ,"three");
    }

    private String selected;

    public String getSelected(){
        return this.selected;
    }
}
