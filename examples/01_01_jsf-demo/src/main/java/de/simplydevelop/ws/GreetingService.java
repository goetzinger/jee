package de.simplydevelop.ws;

import javax.jws.WebService;

@WebService
public interface GreetingService {

    Greeting sayHello(Greeting fromClient);

}
