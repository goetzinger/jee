package de.simplydevelop.ws;

import javax.jws.WebService;

@WebService(endpointInterface = "de.simplydevelop.ws.GreetingService")
public class GreetingServiceImpl implements GreetingService{
    @Override public Greeting sayHello(Greeting fromClient) {
        System.out.println(fromClient.getHello());
        return new Greeting("hello_back_from_server");
    }
}
