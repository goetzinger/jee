package de.simplydevelop.jaxrs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Greeting {
    @NotNull
    @Size(min= 3, max = 100, message = "The message should have a length between 3 and 10")
    private String hello;

    public Greeting(){
    }

    public Greeting(String hello) {
        this.hello = hello;
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }
}
