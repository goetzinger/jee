package de.simplydevelop.jaxrs;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/greetingresponse")
public class GreetingResponseResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getHello(){
        return Response.ok().entity("hello_message").build();
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHelloAsJson(){
        return Response.ok().entity(new Greeting("hello_message")).build();
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insert(Greeting g){
        //add somewhere in DB
        return Response.status(Response.Status.CREATED).entity(g).build();
    }
}
