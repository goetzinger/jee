package de.simplydevelop.jaxrs;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/greetingobject")
public class GreetingObjectResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "Find Greeting Message by message",
            description = "For valid response try integer IDs with value >= 1 and <= 10. Other values will generated exceptions",
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = Greeting.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid message supplied"),
                    @ApiResponse(responseCode = "404", description = "Greeting not found")
            }
    )
    public Greeting getHelloAsJson(
            @Parameter(description = "message of Greeting to fetch", in = ParameterIn.QUERY)  @QueryParam("message") @NotNull @Size(min = 3, max = 10,
            message = "The message should have a length between 3 and 10") String hello) {
        return new Greeting("hello_message");
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Greeting insert(@Valid Greeting g) {
        //add somewhere in DB
        if (g.getHello() == null) {
            System.out.println("Error");
            throw new IllegalArgumentException("hello property is mandatory");
        }
        return g;
    }
}
