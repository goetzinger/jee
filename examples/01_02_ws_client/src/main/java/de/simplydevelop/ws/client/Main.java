package de.simplydevelop.ws.client;

import de.simplydevelop.jee.Greeting;
import de.simplydevelop.jee.GreetingService;
import de.simplydevelop.jee.GreetingServiceImplService;

public class Main {

    public static void main(String[] args) {
        GreetingService service  = new GreetingServiceImplService().getGreetingServiceImplPort();
        Greeting greeting = new Greeting();
        greeting.setHello("Hello");
        System.out.println(service.sayHello(greeting));

    }
}
