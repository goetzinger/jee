package rent_a_car;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named
public class SelectionSample {

	//für die oneListBox
	private String selectedElement;
	
	//für die ManyListBox
	private List<String> selectedElements = new ArrayList<String>();
	
	@PostConstruct
	public void preSelect(){
		this.selectedElement = "Zweites";
		this.selectedElements.add("Drittes");
	}
	
	public String getSelectedElement() {
		return selectedElement;
	}

	public void setSelectedElement(String selectedElement) {
		this.selectedElement = selectedElement;
	}
	
	
	public List<String> getSelectedElements() {
		return selectedElements;
	}

	public void setSelectedElements(List<String> selectedElements) {
		this.selectedElements = selectedElements;
	}

	public List<String> getAllItems(){
		return Arrays.asList("Erstes", "Zweites","Drittes");
	}
}
