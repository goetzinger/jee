package rent_a_car;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import de.jeeschulung.exercise.CoordinateSystemBean;
import de.jeeschulung.exercise.Point;

@Named
@SessionScoped
public class CoordinateSystemViewBean implements Serializable{
	
	@EJB
	private CoordinateSystemBean backendBean; 
	
	public List<Point> getPoints() {
		return backendBean.getPoints();
	}

}
