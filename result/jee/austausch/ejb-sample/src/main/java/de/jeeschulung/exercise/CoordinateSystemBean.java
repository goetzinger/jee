/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jeeschulung.exercise;

import javax.ejb.Stateless;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 *
 * @author martin
 */
@Singleton
public class CoordinateSystemBean {

	private List<Point> points = new ArrayList<>();

	@PostConstruct
	public void init() {
		for (int i = 0; i < 10; i++) {
			points.add(new Point(i, i * 4));
		}
	}

	public List<Point> getPoints() {
		return points;
	}

	// Add business logic below. (Right-click in editor and choose
	// "Insert Code > Add Business Method")

}
