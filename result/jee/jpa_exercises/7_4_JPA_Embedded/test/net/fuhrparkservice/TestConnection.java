package net.fuhrparkservice;

import static org.junit.Assert.*;
import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Filiale;
import net.fuhrparkservice.model.Nutzer;

import org.junit.Test;

public class TestConnection extends AbstractJPATestCase {

	@Override
	public void setUp() throws Exception {

		manager.persist(new FahrzeugType("VW", "Golf", 120, 200));
		manager.persist(new Filiale("Muenchen"));
		manager.persist(new Nutzer("Hans", "Mustermann"));
		manager.flush();
		manager.clear();
	}

	@Test
	public void testFindFahrzeug() {
		assertNotNull(super.manager.find(FahrzeugType.class, 1l).getId());
	}

	@Test
	public void testFindNutzer() {
		// TODO find Nutzer with EntityManager
		assertNotNull(super.manager.find(Nutzer.class, 1l).getId());
	}

	@Test
	public void testFindFiliale() {
		// TODO find filiale with EntityManager
		assertNotNull(super.manager.find(Filiale.class, 1l).getId());
	}

	@Test
	public void testFindModel() {
		assertNotNull(super.manager.find(FahrzeugType.class, 1l).getModel()
				.getFabrikat());
	}

}
