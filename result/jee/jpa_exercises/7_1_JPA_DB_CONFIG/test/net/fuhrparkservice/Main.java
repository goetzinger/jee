package net.fuhrparkservice;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

	public static void main(String[] args) {
		//VORSICHT ist langlaufend => 2 Minuten
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("projectUnit");
		
		//2 nanosekunds
		EntityManager em = emf.createEntityManager();
		
		//DB Connection da?
		boolean isConnectionOpen = em.isOpen();
		System.out.println(isConnectionOpen);
	}

}
