package de.simplydevelop.hello;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
@RequestScoped
public class CarConverter implements Converter {

    @Inject
    private DataAccess dataAccess;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }

        try {
            Long id = Long.valueOf(value);
            Optional<Car> car = dataAccess.find(id);
            return car.orElseThrow(()->new ConverterException("The value is not a valid Car ID: " + value));
        } catch (NumberFormatException e) {
            throw new ConverterException("The value is not a valid Car ID: " + value, e);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }

        if (value instanceof Car) {
            Long id = ((Car) value).getId();
            return (id != null) ? String.valueOf(id) : null;
        } else {
            throw new ConverterException("The value is not a valid Car instance: " + value);
        }
    }

}
