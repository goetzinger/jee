package de.simplydevelop.hello;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
@Named
public class DataAccess {

    private List<Car> carList = new ArrayList<Car>();

    @PostConstruct
    public void initFahrzeugList(){
        this.carList.add(new Car("320d", "BMW",190, 200));
        this.carList.add(new Car("Model 3 LR", "Tesla",170, 250));
        this.carList.add(new Car("EQC", "Mercedes",155, 190));
    }

    public List<Car> getFahrzeuge() {
        return carList;
    }
}
