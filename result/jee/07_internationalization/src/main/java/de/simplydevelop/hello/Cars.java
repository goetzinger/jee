package de.simplydevelop.hello;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@SessionScoped
@Named
public class Cars implements Serializable {

    @Inject
    private DataAccess dataAccess;

    private Car selected;

    public CarClassification[] getClassifications(){
        return CarClassification.values();
    }

    @PostConstruct
    public void initSelected(){
        this.selected = dataAccess.getCars().get(0);
    }

    public Car getSelected() {
        return selected;
    }

    public void setSelected(Car selected) {
        this.selected = selected;
    }

    public String saveChanges(){

        System.out.println(selected);
        return "/index?faces-redirect=true";
    }


}
