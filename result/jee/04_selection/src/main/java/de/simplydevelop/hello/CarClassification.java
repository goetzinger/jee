package de.simplydevelop.hello;

public enum CarClassification {

    Executive,
    MidSize,
    CityCar,
    MPV,
    SUV
}
