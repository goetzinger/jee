package net.fuhrparkservice.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_fahrzeug")
public class Fahrzeug {

	@Id
	@GeneratedValue
	private long id;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private FahrzeugType type;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Filiale standort;

	public Fahrzeug() {
	}

	public Fahrzeug(Filiale standort, FahrzeugType type) {
		setStandort(standort);
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public void setType(FahrzeugType type) {
		this.type = type;
	}

	public FahrzeugType getType() {
		return type;
	}

	public void setStandort(Filiale standort) {
		this.standort = standort;
		this.standort.getFahrzeuge().add(this);
	}

	public Filiale getStandort() {
		return standort;
	}
}
