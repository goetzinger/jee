package net.fuhrparkservice.model;

import javax.persistence.Entity;

@Entity
public class PKW extends FahrzeugType {

	private int tueren;

	public PKW() {
	}

	public PKW(Model model, long ps, long maxKmH, int tueren) {
		super( model, ps, maxKmH);
		this.tueren = tueren;
	}

	public int getTueren() {
		return tueren;
	}

	public void setTueren(int tueren) {
		this.tueren = tueren;
	}

}
