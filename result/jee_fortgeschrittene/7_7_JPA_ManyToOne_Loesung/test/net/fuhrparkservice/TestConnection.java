package net.fuhrparkservice;

import static org.junit.Assert.*;

import java.util.Set;

import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Fahrzeug;
import net.fuhrparkservice.model.Filiale;
import net.fuhrparkservice.model.Model;
import net.fuhrparkservice.model.Nutzer;
import net.fuhrparkservice.model.Person;

import org.junit.Test;

public class TestConnection extends AbstractJPATestCase {

	@Override
	public void setUp() throws Exception {
		
		FahrzeugType fahrzeug = new FahrzeugType("1", new Model("VW", "Golf"), 120, 200);
		manager.persist(fahrzeug);
		Filiale filiale = new Filiale("1", "Muenchen");
		Fahrzeug fahrzeugItem = new Fahrzeug("1", filiale, fahrzeug);
		filiale.addFahrzeug(fahrzeugItem);

		//manager.persist(fahrzeugItem);
		manager.persist(filiale);
		//fahrzeugItem.setStandort(filiale);
		manager.persist(new Nutzer("1", new Person("1", "Hans", "Mustermann")));
		manager.flush();
		manager.clear();
	}

	@Test public void testFindFahrzeug() {
		assertNotNull(super.manager.find(FahrzeugType.class, "1").getId());
	}

	@Test public void testFindNutzer() {
		// TODO find Nutzer with EntityManager
		assertNotNull(super.manager.find(Nutzer.class, "1").getId());
	}

	@Test public void testFindFiliale() {
		// TODO find filiale with EntityManager
		Filiale find = super.manager.find(Filiale.class, "1");
		manager.getTransaction().commit();
		manager.close();
		Set<Fahrzeug> fahrzeuge = find.getFahrzeuge();
		
		int size = fahrzeuge.size();
		assertNotNull(find.getId());
	}

	@Test public void testFindModel() {
		assertNotNull(super.manager.find(FahrzeugType.class, "1").getModel()
				.getFabrikat());
	}

	@Test public void testFindPersonByNutzer() {
		assertNotNull(super.manager.find(Nutzer.class, "1").getPerson()
				.getVorname());
	}

	@Test public void testOneToManyOfFiliale() {
		assertTrue(super.manager.find(Filiale.class, "1").getFahrzeuge()
				.toArray().length > 0);
	}

	@Test public void testManyToOneFahrzeugItem() {
		final Fahrzeug fahrzeug = super.manager.find(Fahrzeug.class, "1");
		assertNotNull(fahrzeug.getStandort());
	}
}
