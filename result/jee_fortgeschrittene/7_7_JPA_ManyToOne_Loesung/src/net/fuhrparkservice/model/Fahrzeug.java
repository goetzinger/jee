package net.fuhrparkservice.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_fahrzeug")
public class Fahrzeug {

	@Id
	private String id;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private FahrzeugType type;
	@ManyToOne()
	private Filiale standort;

	public Fahrzeug() {
	}

	public Fahrzeug(String id, Filiale standort, FahrzeugType type) {
		this.id = id;
		setStandort(standort);
		this.type = type;
	}

	public void setType(FahrzeugType type) {
		this.type = type;
	}

	public FahrzeugType getType() {
		return type;
	}

	public void setStandort(Filiale standort) {
		if(this.standort != null)
			this.standort.getFahrzeuge().remove(this);
		this.standort = standort;
		if (standort != null) {
			standort.addFahrzeug(this);
		}
	}

	public Filiale getStandort() {
		return standort;
	}
}
