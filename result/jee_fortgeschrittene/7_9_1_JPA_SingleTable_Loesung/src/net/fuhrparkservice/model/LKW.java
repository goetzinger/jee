package net.fuhrparkservice.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("LKW")
public class LKW extends FahrzeugType {

	private int maxZuladung;
	
	public LKW() {
		super();
	}

	public LKW(String id, Model model, long ps, long maxKmH, int maxZuladung) {
		super(id, model, ps, maxKmH);
		this.maxZuladung = maxZuladung;
	}

	public int getMaxZuladung() {
		return maxZuladung;
	}

	public void setMaxZuladung(int maxZuladung) {
		this.maxZuladung = maxZuladung;
	}
}
