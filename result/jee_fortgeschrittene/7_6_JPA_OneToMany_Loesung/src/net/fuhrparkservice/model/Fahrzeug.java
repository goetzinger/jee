package net.fuhrparkservice.model;

import java.beans.ConstructorProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "tbl_fahrzeug")
public class Fahrzeug {

	@Id
	private String id;
	@ManyToOne
	private FahrzeugType type;
	@Transient
	private Filiale standort;
	
	public Fahrzeug() {
	
	}

	public Fahrzeug(String id, Filiale standort, FahrzeugType type) {
		this.id = id;
		this.standort = standort;
		this.type = type;
	}

	public void setType(FahrzeugType type) {
		this.type = type;
	}

	public FahrzeugType getType() {
		return type;
	}

	public void setStandort(Filiale standort) {
		this.standort = standort;
	}

	public Filiale getStandort() {
		return standort;
	}
}
