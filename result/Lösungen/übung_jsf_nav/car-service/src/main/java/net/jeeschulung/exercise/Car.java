package net.jeeschulung.exercise;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = Car.QUERY_FIND_ALL,query = Car.QUERY_FIND_ALL),
	@NamedQuery(name = Car.QUERY_FIND_BY_BRAND,query = Car.QUERY_FIND_BY_BRAND),
	@NamedQuery(name = Car.QUERY_FIND_BY_LOCATION_ID,query = Car.QUERY_FIND_BY_LOCATION_ID),
	@NamedQuery(name=Car.QUERY_BY_CLASSIFICATION,query = Car.QUERY_BY_CLASSIFICATION)
})
public class Car{

	static final String PARAM_CLASSIFICATION = "classification";
	static final String PARAM_LOCATION_NAME = "location_name";
	static final String PARAM_BRAND = "brand";
	public static final String QUERY_FIND_ALL = "SELECT c FROM Car c";
	public static final String QUERY_FIND_BY_BRAND = "SELECT c FROM Car c where c.brand = :" + PARAM_BRAND;
	public static final String PARAM_LOCATION_ID = "locationId";
	public static final String QUERY_FIND_BY_LOCATION_ID = "SELECT c FROM Car c where c.location.id = :" + PARAM_LOCATION_ID;
	public static final String QUERY_BY_CLASSIFICATION = "SELECT c FROM Car c, IN (c.types) cl WHERE cl IN :" + PARAM_CLASSIFICATION ;
	public static final String QUERY_BY_CLASSIFICATION_AND_LOCATION_NAME = "SELECT c FROM Car c, IN (c.types) cl WHERE c.location.name = :"
			+PARAM_LOCATION_NAME + " AND cl IN :" + PARAM_CLASSIFICATION ;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String model;
    private String brand;
    private int hp;
    private int maxKpH;
    private BigDecimal price;
    
    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    private List<CarClassification> types = new ArrayList<>();
    
    @ManyToOne
    private Location location;

    public Car(){

    }
    public Car(long id, String model, String brand, int hp, int maxKpH){
        this.id = id;
        this.model = model;
        this.brand = brand;
        this.hp = hp;
        this.maxKpH = maxKpH;
    }

    public long getId() {
        return id;
    }
    
    public void setId(long id) {
		this.id = id;
	}

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
    
    public BigDecimal getPrice() {
		return price;
	}
    
    public void setPrice(BigDecimal price) {
		this.price = price;
	}

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMaxKpH() {
        return maxKpH;
    }

    public void setMaxKpH(int maxKpH) {
        this.maxKpH = maxKpH;
    }

    public List<CarClassification> getTypes() {
        return types;
    }

    public void setTypes(List<CarClassification> types) {
        this.types = types;
    }
    
   
    
    public void setLocation(Location location) {
		this.location = location;
	}
    
    public Location getLocation() {
		return location;
	}

    @Override public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                ", hp=" + hp +
                ", maxKpH=" + maxKpH +
                ", types=" + types +
                '}';
    }
}
