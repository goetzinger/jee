package net.jeeschulung.exercise;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.ejb.MessageDriven;
import javax.ejb.ActivationConfigProperty;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/queue/testQueue") })
public class SomeDrivenBean implements MessageListener {

	@Override
	public void onMessage(Message message) {
		System.out.println(message);

	}

}
