package de.simplydevelop.hello;

import java.util.Optional;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

import net.jeeschulung.exercise.CarDTO;
import net.jeeschulung.exercise.CarService;

@Named("carConverter")
@RequestScoped
public class CarConverter implements Converter {

    @Inject
    private CarService dataAccess;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
   
        if (value == null || value.isEmpty()) {
            return null;
        }

        try {
            Long id = Long.valueOf(value);
            Optional<CarDTO> car = dataAccess.findById(id);
            return car.orElseThrow(()->new ConverterException("The value is not a valid Car ID: " + value));
        } catch (NumberFormatException e) {
            throw new ConverterException("The value is not a valid Car ID: " + value, e);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }

        if (value instanceof CarDTO) {
            Long id = ((CarDTO) value).getId();
            return (id != null) ? String.valueOf(id) : null;
        } else {
            throw new ConverterException("The value is not a valid Car instance: " + value);
        }
    }

}
