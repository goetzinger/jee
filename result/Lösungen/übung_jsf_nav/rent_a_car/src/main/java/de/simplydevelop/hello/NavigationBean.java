package de.simplydevelop.hello;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ConversationScoped
public class NavigationBean implements Serializable{
	
	@Inject
	private Conversation conversation;
	
	@PostConstruct
	public void conversationBegin() {
		conversation.begin();
	}
	
	boolean isDone;

	public NavigationBean() {
		System.out.println("Neue Bean wird erzeugt " + this);
	}

	public boolean isDone() {
		return isDone;
	}

	public void setDone(boolean isDone) {
		System.out.println(this);
		System.out.println("SET IS DONE " + isDone);
		this.isDone = isDone;
	}

	public String back() {
		return "back";
	}
	
	
	public String end() {
		conversation.end();
		return "end";
	}

	public String next() {
		
		System.out.println("NEXT " + isDone);
		return isDone ? Outcome.FINISHED.toString() : Outcome.NOT_FINISHED.toString();
	}
}
