package de.simplydevelop.hello;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import javax.inject.Named;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.jeeschulung.exercise.CarClassification;
import net.jeeschulung.exercise.CarDTO;
import net.jeeschulung.exercise.CarService;

@Named
@ApplicationScoped
public class CarServiceClient implements CarService {

	private WebTarget target;

	@PostConstruct
	public void initRestClient() {
		this.target = ClientBuilder.newClient().register(MyAuthenticationFilter.class)
				.target("http://localhost:8080/api/cars");
	}

	@Override
	public List<CarDTO> findAll() {
		List<CarDTO> cars = target.request(MediaType.APPLICATION_JSON).get(new GenericType<List<CarDTO>>() {
		});
		return cars;
	}
	
	

	@Override
	public Optional<CarDTO> findById(Long id) {
		try {
			CarDTO car = target.path(String.valueOf(id)).request(MediaType.APPLICATION_JSON).get(CarDTO.class);
			return Optional.of(car);
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	@Override
	public CarDTO update(CarDTO toUpdate) {
		return target.path(String.valueOf(toUpdate.getId())).request(MediaType.APPLICATION_JSON).put(Entity.json(toUpdate),
				CarDTO.class);
	}

	@Override
	public CarDTO insert(CarDTO toInsert) {
		return target.request(MediaType.APPLICATION_JSON).post(Entity.json(toInsert), CarDTO.class);
		
	}

	@Override
	public void delete(long id) {
		target.path(String.valueOf(id)).request(MediaType.APPLICATION_JSON).delete();

	}

	@Override
	public List<CarDTO> findByBrand(String brand) {
		List<CarDTO> cars = target.queryParam("brand", brand).request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<CarDTO>>() {
				});
		return cars;
	}

	@Override
	public List<CarDTO> findByLocation(long locationId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CarDTO> findByClassification(List<CarClassification> carClassification) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CarDTO> findByClassificationAndLocation(List<CarClassification> carClassification, String location) {
		WebTarget targetIntern = target.queryParam("location", location)
			.queryParam("classification", carClassification.toArray());
		
		List<CarDTO> cars = targetIntern
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<CarDTO>>() {
		});
		return cars;
	}

}
