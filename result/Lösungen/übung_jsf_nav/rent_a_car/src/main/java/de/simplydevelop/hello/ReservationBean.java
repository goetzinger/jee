package de.simplydevelop.hello;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import net.jeeschulung.exercise.CarClassification;
import net.jeeschulung.exercise.CarDTO;

@Named
@ConversationScoped
public class ReservationBean implements Serializable {

	@Inject
	private Conversation conversation;
	
	@Inject
	private CarServiceClient carService;

	private String from;

	private String to;

	private String locationName;

	private List<CarClassification> typesToSearch = new ArrayList<CarClassification>();
	
	private CarDTO selected;

	private List<CarDTO> matchingCars;

	private BigDecimal price;
	
	public CarDTO getSelected() {
		return selected;
	}
	
	public List<CarDTO> getMatchingCars() {
		return matchingCars;
	}

	public CarClassification[] getClassifications() {
		return CarClassification.values();
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public List<CarClassification> getTypesToSearch() {
		return typesToSearch;
	}

	public void setTypesToSearch(List<CarClassification> typesToSearch) {
		this.typesToSearch = typesToSearch;
	}

	public String search() {
		if(isNotBlank(from) && isNotBlank(to) && !typesToSearch.isEmpty() && isNotBlank(locationName)) {
			conversation.begin();
			this.matchingCars = carService.findByClassificationAndLocation(typesToSearch, locationName);
			return "successful";
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage("Nicht alle Eingaben wurden getätigt"));
			return "warning";
		}
	}
	
	public String showSummary() {
		if(this.selected != null) {
			calculatePrice();
			return "successful";
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage("Wählen Sie ein Fahrzeug aus."));
			return "warning";
		}
	}


	private void calculatePrice() {
		this.price = this.selected.getPrice();
		
	}
	
	public BigDecimal getPrice() {
		return price;
	}

	private boolean isNotBlank(String tocheck) {
		return tocheck != null && !tocheck.isBlank();
	}
	
}
