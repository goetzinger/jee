package net.jeeschulung.exercise;

import java.util.Calendar;

public class ReservationDTO {
	
	private int id;
	
	private Calendar from;
	
	private Calendar to;
	
	private long carId;
	
	private long pickupLocationId;
	
	private long returnLocationId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Calendar getFrom() {
		return from;
	}

	public void setFrom(Calendar from) {
		this.from = from;
	}

	public Calendar getTo() {
		return to;
	}

	public void setTo(Calendar to) {
		this.to = to;
	}

	public long getCarId() {
		return carId;
	}

	public void setCarId(long carId) {
		this.carId = carId;
	}

	public long getPickupLocationId() {
		return pickupLocationId;
	}

	public void setPickupLocationId(long pickupLocationId) {
		this.pickupLocationId = pickupLocationId;
	}

	public long getReturnLocationId() {
		return returnLocationId;
	}

	public void setReturnLocationId(long returnLocationId) {
		this.returnLocationId = returnLocationId;
	}
	
	

}
