package net.jeeschulung.exercise;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class CarDTO implements Serializable{

    private long id;
    @NotNull
    @Size(min=1, max=255)
    private String model;
    @NotNull
    @Size(min=2, max=255)
    private String brand;
    @Min(1)
    private int hp;
    @Min(1)
    private int maxKpH;
    
    @Digits(integer=6,fraction = 2)
    private BigDecimal price;

    private List<CarClassification> types = new ArrayList<>();

    public CarDTO(){

    }
    public CarDTO(long id, String model, String brand, int hp, int maxKpH){
        this.id = id;
        this.model = model;
        this.brand = brand;
        this.hp = hp;
        this.maxKpH = maxKpH;
    }

    public long getId() {
        return id;
    }
    
    public void setId(long id) {
		this.id = id;
	}

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMaxKpH() {
        return maxKpH;
    }

    public void setMaxKpH(int maxKpH) {
        this.maxKpH = maxKpH;
    }

    public List<CarClassification> getTypes() {
        return types;
    }

    public void setTypes(List<CarClassification> types) {
        this.types = types;
    }
    
    public BigDecimal getPrice() {
		return price;
	}
    
    public void setPrice(BigDecimal price) {
		this.price = price;
	}

    @Override public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                ", hp=" + hp +
                ", maxKpH=" + maxKpH +
                ", price=" + price +
                ", types=" + types +
                '}';
    }
}
