package net.jeeschulung.exercise;

public enum CarClassification {

    Executive,
    MidSize,
    CityCar,
    MPV,
    SUV
}
