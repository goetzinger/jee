package de.simplydevelop.hello;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import net.jeeschulung.exercise.CarClassification;
import net.jeeschulung.exercise.CarDTO;
import net.jeeschulung.exercise.CarService;

@SessionScoped
@Named
public class Cars implements Serializable {

    @Inject
    private CarService dataAccess;
    
    private CarDTO selected;

    public CarClassification[] getClassifications(){
        return CarClassification.values();
    }
    
    public List<CarDTO> getCars(){
    	return dataAccess.findAll();
    }

    public CarDTO getSelected() {
        return selected;
    }

    public void setSelected(CarDTO selected) {
        this.selected = selected;
    }

    public String saveChanges(){
    	this.dataAccess.update(selected);
        System.out.println(selected);
        return "/cars?faces-redirect=true";
    }
    public boolean hasAdminPermission() {
    	return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("admin");
    }


}
