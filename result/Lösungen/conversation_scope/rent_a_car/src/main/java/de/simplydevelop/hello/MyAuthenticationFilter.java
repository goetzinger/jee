package de.simplydevelop.hello;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

public class MyAuthenticationFilter implements ClientRequestFilter{

	@Override
	public void filter(ClientRequestContext restCallRequest) throws IOException {
		//hole header aus JSF Request
		String basicHeader = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("Authorization");
		//packe den Wert in den neuen RestCall zur REST API rein
		restCallRequest.getHeaders().add("Authorization", basicHeader);
		
	}

}
