/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jeeschulung.exercise;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.JButton;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean2;
import org.apache.commons.collections.ListUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Singleton;

/**
 *
 * @author J.A.R.V.I.S
 */
@Stateless
@Local(CarService.class)
@Remote(CarServiceRemote.class)
@PermitAll
public class CarServiceBean implements CarService {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<CarDTO> findAll() {
		return manager.createQuery(Car.QUERY_FIND_ALL, Car.class).getResultList().stream().map(this::mapToDto)
				.collect(Collectors.toList());
	}

	@Override
	public Optional<CarDTO> findById(Long id) {
		Car find = manager.find(Car.class, id);
		return Optional.ofNullable(mapToDto(find));
	}

	private CarDTO mapToDto(Car car) {
		if (car == null)
			return null;
		CarDTO toReturn = new CarDTO();
		toReturn.getTypes().addAll(car.getTypes());
		toReturn.setBrand(car.getBrand());
		toReturn.setModel(car.getModel());
		toReturn.setHp(car.getHp());
		toReturn.setMaxKpH(car.getMaxKpH());
		toReturn.setId(car.getId());
		return toReturn;
	}

	@RolesAllowed({ "admin" })
	@Override
	public CarDTO update(CarDTO changedCar) {

		Car car = manager.find(Car.class, changedCar.getId());
		car.setBrand(changedCar.getBrand());
		car.setModel(changedCar.getModel());
		car.setHp(changedCar.getHp());
		car.setMaxKpH(changedCar.getMaxKpH());
		car.getTypes().clear();
		car.getTypes().addAll(changedCar.getTypes());

		return mapToDto(car);
	}

	@RolesAllowed({ "admin" })
	@Override
	public CarDTO insert(CarDTO toInsert) {

		Car car = new Car();
		car.setBrand(toInsert.getBrand());
		car.setModel(toInsert.getModel());
		car.setHp(toInsert.getHp());
		car.setMaxKpH(toInsert.getMaxKpH());
		car.getTypes().addAll(toInsert.getTypes());
		manager.persist(car);
		return mapToDto(car);
	}

	@Override
	public void delete(long id) {
		manager.remove(manager.getReference(Car.class, id));
	}

	@Override
	public List<CarDTO> findByBrand(String brand) {
		return manager.createQuery(Car.QUERY_FIND_BY_BRAND, Car.class).setParameter(Car.PARAM_BRAND, brand)
				.getResultList().stream().map(c -> mapToDto(c)).collect(Collectors.toList());

	}

	@Override
	public List<CarDTO> findByLocation(long locationId) {
		return manager.createNamedQuery(Car.QUERY_FIND_BY_LOCATION_ID,Car.class)
				.setParameter(Car.PARAM_LOCATION_ID,locationId).getResultList().stream().map(this::mapToDto).collect(Collectors.toList());
	}

}
