/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jeeschulung.exercise;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.JButton;
import javax.transaction.Transactional;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean2;
import org.apache.commons.collections.ListUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Singleton;

/**
 *
 * @author J.A.R.V.I.S
 */
@Stateless
@LocalBean
@PermitAll
public class LocationServiceBean {
	
	@PersistenceContext
	private EntityManager manager;

	
	public List<LocationDTO> findAll() {
		return manager.createNamedQuery(Location.QUERY_FIND_ALL, Location.class).
				getResultList().stream().map(this::mapToDto)
				.collect(Collectors.toList());
	}

	public Optional<LocationDTO> findById(Long id) {
		Location find = manager.find(Location.class, id);
		return Optional.ofNullable(mapToDto(find));
	}

	private LocationDTO mapToDto(Location entity) {
		if (entity == null)
			return null;
		LocationDTO dto = new LocationDTO();
		dto.setName(entity.getName());
		dto.setAddress(entity.getAddress());
		dto.setId(entity.getId());
		return dto;
	}
	
	private LocationDTO mapToEntity(LocationDTO dto, Location entity) {
		if (dto == null)
			return null;
		entity.setName(dto.getName());
		entity.setAddress(dto.getAddress());
		entity.setId(dto.getId());
		return dto;
	}

	@RolesAllowed({ "admin" })
	public LocationDTO update(LocationDTO updatedValue) {

		Location entity = manager.find(Location.class, updatedValue.getId());
		mapToEntity(updatedValue, entity);
		return mapToDto(entity);
	}

	@RolesAllowed({ "admin" })
	public LocationDTO insert(LocationDTO toInsert) {

		
		Location entity = new Location();
		mapToEntity(toInsert, entity);
		manager.persist(entity);
		return mapToDto(entity);
	}

	public void delete(long id) {
		manager.remove(manager.getReference(Location.class, id));
	}

	public Optional<LocationDTO> findByCar(long carId) {
		return manager.createNamedQuery(Location.QUERY_FIND_BY_CAR,Location.class)
		.setParameter(Location.PARAM_CAR_ID, carId).getResultList().stream().map(this::mapToDto).findFirst();
	}

	public LocationDTO addToCar(long carId, LocationDTO locationDTO) {
		Car car = manager.find(Car.class,carId);
		Location location = manager.find(Location.class, locationDTO.getId());
		car.setLocation(location);
		return mapToDto(location);
	}


}
