package de.simplydevelop.api;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

public class MyAuthenticationFilter implements ClientRequestFilter {

	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		String string = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("Authorization");
		requestContext.getHeaders().add("Authorization", string);
		
	}

}
