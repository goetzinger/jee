package rent_a_car;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import de.jeeschulung.exercise.Point;

@RequestScoped
@Named
public class Page2Bean {
	
	private Point pointToShow;
	
	public Point getPointToShow() {
		return pointToShow;
	}
	
	public void setPointToShow(Point pointToShow) {
		this.pointToShow = pointToShow;
	}
	
	public String back() {
		return "/page1?faces-redirect=true";
	}

}
