package rent_a_car;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Inject;
import javax.inject.Named;

import de.jeeschulung.exercise.CoordinateSystemBean;
import de.jeeschulung.exercise.Point;

@Named
@ApplicationScoped
public class XToPointConverter implements Converter{
	
	@EJB
	private CoordinateSystemBean coordSystem;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			int xCoordinate = Integer.parseInt(value);
			for(Point p : coordSystem.getPoints()) {
				if(p.getX() == xCoordinate)
					return p;
			}
		}catch(NumberFormatException e) {
			throw new ConverterException("X coordinate was not a number " +value);
		}
		throw new ConverterException("X coordinate does not belong to a valid Point object: " +value);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		
		if(value == null)
			return "";
		if (value instanceof Point){
			Point p = (Point) value;
			return ""+p.getX();
		}
		throw new ConverterException("Object is not of type Point: " +value);
	}
	
	

}
