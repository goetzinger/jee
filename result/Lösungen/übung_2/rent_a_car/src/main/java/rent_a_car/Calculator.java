package rent_a_car;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named("calc")
@SessionScoped
public class Calculator implements Serializable{
	
	private int input = 0;
	
	private int result = 0;

	public int getInput() {
		return input;
	}

	public void setInput(int input) {
		this.input = input;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
	
	public String times2() {
		this.result = input * 2;
		return "/index";
	}
	
	

}
