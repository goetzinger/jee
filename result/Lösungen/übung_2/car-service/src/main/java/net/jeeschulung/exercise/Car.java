package net.jeeschulung.exercise;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Car{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String model;
    private String brand;
    private int hp;
    private int maxKpH;
    
    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    private List<CarClassification> types = new ArrayList<>();

    public Car(){

    }
    public Car(long id, String model, String brand, int hp, int maxKpH){
        this.id = id;
        this.model = model;
        this.brand = brand;
        this.hp = hp;
        this.maxKpH = maxKpH;
    }

    public long getId() {
        return id;
    }
    
    public void setId(long id) {
		this.id = id;
	}

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMaxKpH() {
        return maxKpH;
    }

    public void setMaxKpH(int maxKpH) {
        this.maxKpH = maxKpH;
    }

    public List<CarClassification> getTypes() {
        return types;
    }

    public void setTypes(List<CarClassification> types) {
        this.types = types;
    }

    @Override public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                ", hp=" + hp +
                ", maxKpH=" + maxKpH +
                ", types=" + types +
                '}';
    }
}
