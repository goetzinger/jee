package de.simplydevelop.api;

import java.lang.annotation.Retention;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import net.jeeschulung.exercise.CarDTO;
import net.jeeschulung.exercise.CarService;

@Named
@RequestScoped
@Path("/cars")
public class CarsResource {
	
	@EJB
	private CarService carService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@QueryParam("brand") String brand){
		List<CarDTO> toReturn = null;
		if(brand != null && !brand.isBlank())
			toReturn =  carService.findByBrand(brand);
		else
			toReturn =  carService.findAll();
		return Response.status(toReturn.isEmpty() ? Status.NO_CONTENT : Status.OK).entity(toReturn).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CarDTO insert(CarDTO toInsert) {
		return carService.insert(toInsert);
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CarDTO update(@PathParam("id")long id, CarDTO toInsert) {
		if(toInsert.getId() == 0)
			toInsert.setId(id);
		return carService.update(toInsert);
	}
	
	@DELETE
	@Path("/{id}")
	public void delete(@PathParam("id")long id) {	
		carService.delete(id);
	}

}
