package net.jeeschulung.exercise;

import java.util.List;
import java.util.Optional;

public interface CarService {

	List<CarDTO> findAll();

	Optional<CarDTO> findById(Long id);

	CarDTO update(CarDTO car);

	CarDTO insert(CarDTO toInsert);

	void delete(long id);

	List<CarDTO> findByBrand(String brand);

}