package net.jeeschulung.exercise;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CarDTO implements Serializable{

    private long id;
    private String model;
    private String brand;
    private int hp;
    private int maxKpH;

    private List<CarClassification> types = new ArrayList<>();

    public CarDTO(){

    }
    public CarDTO(long id, String model, String brand, int hp, int maxKpH){
        this.id = id;
        this.model = model;
        this.brand = brand;
        this.hp = hp;
        this.maxKpH = maxKpH;
    }

    public long getId() {
        return id;
    }
    
    public void setId(long id) {
		this.id = id;
	}

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMaxKpH() {
        return maxKpH;
    }

    public void setMaxKpH(int maxKpH) {
        this.maxKpH = maxKpH;
    }

    public List<CarClassification> getTypes() {
        return types;
    }

    public void setTypes(List<CarClassification> types) {
        this.types = types;
    }

    @Override public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                ", hp=" + hp +
                ", maxKpH=" + maxKpH +
                ", types=" + types +
                '}';
    }
}
