package de.simplydevelop.hello;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.xml.ws.BindingProvider;

import de.simplydevelop.api.LocationsWebservice;
import de.simplydevelop.api.LocationsWebserviceService;

@SessionScoped
@Named
public class Locations implements Serializable{

	public List<de.simplydevelop.api.LocationDTO> getLocations() throws MalformedURLException {
		
		
		LocationsWebservice service = new LocationsWebserviceService().getLocationsWebservicePort();
		((BindingProvider)service).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "user1");
		((BindingProvider)service).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "a");
		
		return service.getAll();
	}

}
