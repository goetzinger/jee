package de.simplydevelop.api;

import java.util.List;
import java.util.Optional;

import javax.ejb.EJB;
import javax.jws.WebService;

import net.jeeschulung.exercise.LocationDTO;
import net.jeeschulung.exercise.LocationServiceBean;

@WebService
public class LocationsWebservice {

	@EJB
	private LocationServiceBean locationService;

	public List<LocationDTO> getAll() {
		List<LocationDTO> toReturn = null;
		toReturn = locationService.findAll();
		return toReturn;
	}

	public LocationDTO insert(LocationDTO toInsert) {
		return locationService.insert(toInsert);
	}

	public LocationDTO update(long id, LocationDTO toInsert) {
		if (toInsert.getId() == 0)
			toInsert.setId(id);
		return locationService.update(toInsert);
	}

	public void delete(long id) {
		locationService.delete(id);
	}

	public LocationDTO getLocationOfCar(long carId) {
		Optional<LocationDTO> toReturn = null;
		toReturn = locationService.findByCar(carId);
		return toReturn.orElse(null);
	}

	public LocationDTO setLocationOfCar(long carId, LocationDTO location) {

		LocationDTO toReturn = locationService.addToCar(carId, location);
		return toReturn;
	}

}
