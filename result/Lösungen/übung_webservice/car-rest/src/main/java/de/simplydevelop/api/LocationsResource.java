package de.simplydevelop.api;

import java.lang.annotation.Retention;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import net.jeeschulung.exercise.LocationDTO;
import net.jeeschulung.exercise.LocationServiceBean;

@Named
@RequestScoped
@Path("/")
public class LocationsResource {

	@EJB
	private LocationServiceBean locationService;

	@GET
	@Path("/locations")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		List<LocationDTO> toReturn = null;
		toReturn = locationService.findAll();
		return Response.status(toReturn.isEmpty() ? Status.NO_CONTENT : Status.OK).entity(toReturn).build();
	}

	@POST
	@Path("/locations")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public LocationDTO insert(@Valid LocationDTO toInsert) {
		return locationService.insert(toInsert);
	}

	@PUT
	@Path("/locations/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public LocationDTO update(@PathParam("id") long id, @Valid LocationDTO toInsert) {
		if (toInsert.getId() == 0)
			toInsert.setId(id);
		return locationService.update(toInsert);
	}

	@DELETE
	@Path("/locations/{id}")
	public void delete(@PathParam("id") long id) {
		locationService.delete(id);
	}

	@GET
	@Path("cars/{id}/locations")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLocationOfCar(@PathParam("id") long carId) {
		Optional<LocationDTO> toReturn = null;
		toReturn = locationService.findByCar(carId);
		return Response.status(toReturn.isEmpty() ? Status.NO_CONTENT : Status.OK).entity(toReturn).build();
	}

	@POST
	@Path("cars/{id}/locations")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LocationDTO getLocationOfCar(@PathParam("id") long carId, LocationDTO location) {

		LocationDTO toReturn = locationService.addToCar(carId, location);
		return toReturn;
	}

}
