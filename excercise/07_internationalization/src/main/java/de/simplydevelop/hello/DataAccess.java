package de.simplydevelop.hello;

import de.simplydevelop.NewSessionBean;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
@Named
public class DataAccess {

    @EJB
    private NewSessionBean bean;

    private List<Car> carList = new ArrayList<Car>();

    @PostConstruct
    public void initFahrzeugList(){
        System.out.println(bean != null);
        this.carList.add(new Car(1,"320d", "BMW",190, 200));
        this.carList.add(new Car(2,"Model 3 LR", "Tesla",170, 250));
        this.carList.add(new Car(3,"EQC", "Mercedes",155, 190));
    }

    public List<Car> getCars() {
        this.bean.businessMethod();
        return carList;
    }

    public Optional<Car> find(Long id) {
        return this.carList.stream().filter(car -> id.equals(car.getId())).findFirst();
    }
}
