package net.fuhrparkservice;

import static org.junit.Assert.*;

import org.junit.Test;

import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Nutzer;
import net.fuhrparkservice.model.Person;
import net.fuhrparkservice.model.PersonKey;

public class TestConnection extends AbstractJPATestCase {

	private Long id;
	private Long nutzerId;

	@Override
	public void setUp() throws Exception {
		
		FahrzeugType fahrzeug = new FahrzeugType("VW", "Golf", 120, 200);
		manager.persist(fahrzeug);
		
		Person p = new Person();
		p.setFirstname("A");
		p.setLastname("B");
		
		manager.persist(p);
		
		this.id = fahrzeug.getId();
		Nutzer n = new Nutzer("a", "b");
		manager.persist(n);
		this.nutzerId = n.getId();
		manager.flush();
		manager.clear();
	}

	@Test public void testFind() {
		manager.find(Person.class, new PersonKey("A","B"));
		assertEquals(id, super.manager.find(FahrzeugType.class, id).getId());
		assertEquals(nutzerId, super.manager.find(Nutzer.class, nutzerId)
				.getId());
	}

}
