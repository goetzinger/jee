/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.simplydevelop;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author
 */
@Stateless
@LocalBean
public class NewSessionBean {

    @RolesAllowed("mygroup")
    public void businessMethod() {
    }

}
