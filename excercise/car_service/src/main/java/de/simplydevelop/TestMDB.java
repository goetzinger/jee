package de.simplydevelop;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;

@MessageDriven(activationConfig =
        {
                @ActivationConfigProperty(propertyName="destinationType", propertyValue="javax.jms.Queue"),
                @ActivationConfigProperty(propertyName="destination", propertyValue="queue/testQueue"),
                @ActivationConfigProperty(propertyName="DLQMaxResent", propertyValue="10")
        })
public class TestMDB implements MessageListener {
    @Override public void onMessage(Message message) {
        System.out.println(message);
    }
}
